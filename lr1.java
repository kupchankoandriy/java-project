package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        char of;
        System.out.println("чи ви хочете користуватись програмою y/n ? ");
        of = sc.next().charAt(0);
        while (of=='y')
       {
        System.out.println("Ведіть номер функції від 1-4 : ");
        int func = in.nextInt();
        switch (func)
        {
        case 1:
            System.out.println("Ведіть  яку форму if else хочете вибрати скорочену форму чи повну : ");
            int q = in.nextInt();
            System.out.println("Ведіть дані");
            System.out.println("Ведіть у ");
            int y = in.nextInt();
            System.out.println("Ведіть z ");
            int z = in.nextInt();
            fun1(y,z,q);
            break;
        case 2:
            System.out.println("Ведіть 3 числа");
            System.out.println("число 1 : ");
            int a = in.nextInt();
            System.out.println("число 2 : ");
            int b = in.nextInt();
            System.out.println("число 3 : ");
            int c = in.nextInt();
            fun2(a,b,c);
            break;
        case 3:
            System.out.println("ведіть номер квартири : ");
            int d = in.nextInt();
            fun3(d);
            break;
        case 4:
            System.out.println("Ведіть дані");
            System.out.println("ведіть х ");
            double x = in.nextDouble();
            System.out.println("ведіть у ");
            double y1 = in.nextDouble();
            fun4(x,y1);
            break;
        default:
            System.out.println("  Під таким номером функції не існує . ");
        }
           System.out.println("чи ви хочете користуватись програмою y/n ? ");
           of = sc.next().charAt(0);
       }
    }

    static void fun1( int y, int z,int q)
    {
        switch (q)
        {
            case 1:
                double x ;
                try
                {
                    if(y != 2 && y != -8 && y != 10)throw new Exception("Точка не належить в область значень функції");
                    if(y == -8)
                        if (z >= -16) throw new Exception("Помилка. При z>=-16 функція результату обчислення немає");
                            x = Math.sqrt((3 * y) / (z - 2 * y)) - y * z;
                    if(y == 2)
                        x = y * z - 7 * z;
                    if(y == 10)
                        x = (y / 2) + 15 * z;
                    String r = String.format("%.2f",x);
                    System.out.println("Результат обчислення фунції  =  " + r + " при y = " + y + " при z = " + z);
                }
                catch (Exception ex)
                {
                    System.out.println(ex.getMessage());
                }
                break;
            case 2:
                if (y == (-8))
                {
                    try {
                        if (z >= -16) throw new Exception("Помилка. При z>=-16 функція результату обчислення немає");
                        {
                            x = Math.sqrt((3 * y) / (z - 2 * y)) - y * z;
                            String res = String.format("%.2f",x);
                            System.out.println("Результат обчислення фунції  =  " + res + " y = " + y + " z = " + z);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.out.println(ex.getMessage());
                    }
                }
                if (y == 10) {
                    x = (y / 2) + 15 * z;
                    String re = String.format("%.2f",x);
                    System.out.println("Результат обчислення фунції =  " + re + " при y = " + y + " при z = " + z);
                }
                else if (y == 2) {
                    x = y * z - 7 * z;
                    String r = String.format("%.2f",x);
                    System.out.println("Результат обчислення фунції  =  " + r + " при y = " + y + " при z = " + z);
                }
                else
                {
                    System.out.println("Точка не належить в область значень функції");
                }
                break;

            default:
                break;
        }

    }

    static void fun2(int a, int b, int c)
    {
     if (a > b && b > c)
     {
         System.out.println("числа склали спадну послідовніть тому їх змінюють на протилежним за знаком числа "+ "\t" +(a*-1)+ "\t" +(b*-1)+ "\t" + (c*-1));
     }
     else
         System.out.println("числа не склали спадну послідовніть тому виводиться сума квадратів цих чисел " + "\t" + (a*a+b*b+c*c));
    }

    static void fun3(int d)
    {
        switch(d){

            case 1,2,3,4,5:
                System.out.println(" ця квартира ноходиться 1 поверх ");
                break;
            case 6,7,8,9,10:
                System.out.println("ця квартира ноходиться 2 поверх ");
                break;
            case 11,12,13,14,15:
                System.out.println("ця квартира ноходиться 3 поверх ");
                break;
            case 16,17,18,19,20:
                System.out.println("ця квартира ноходиться 4 поверх ");
                break;
            case 21,22,23,24,25:
                System.out.println(" ця квартира ноходиться 5 поверх ");
                break;
            case 26,27,28,29,30:
                System.out.println(" ця квартира ноходиться 6 поверх ");
                break;
            case 31,32,33,34,35:
                System.out.println(" ця квартира ноходиться 7 поверх ");
                break;
            case 36,37,38,39,40:
                System.out.println(" ця квартира ноходиться 8 поверх ");
                break;
            case 41,42,43,44,45:
                System.out.println(" ця квартира ноходиться 9 поверх ");
                break;
                default:
                    System.out.println("Такої квартири в цьому будинку немає ");

        }
    }

    static void fun4(double x,double y)
    {   try
        {
            double  q = Math.pow(x,2) + 4;
            if(q == 0)throw new Exception("Помилка. Знаменик x ^ 2 + 4 дорівнює 0 а ділення на нуль не можмиве . ");
            {   double w = Math.pow(Math.E,(x*(-1))-2) + 1/q;
                if(w == 0)throw new Exception("Помилка. Знаменик e ^ -x-2 + 1 / x^2 + 4 дорівнює 0 а ділення на нуль не можмиве . ");
                {
                    double a = (1+y) * ( (x + y / q) / w);
                    String res = String.format("%.2f",a);
                    System.out.printf("Результат обчислення = " + res + "\n");
                }
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

import org.junit.Test;

import static org.junit.Assert.*;

public class Arr_funTest {

    @Test
    public void mins() {
        Arr_fun arr_fun = new Arr_fun();
        int[] arr = new int[] { -1, 2, -3, 5 };
        int q = arr_fun.Mins(arr);
        int expected = 2;
        assertEquals(expected,q);
    }

    @Test
    public void minSum() {
        Arr_fun arr_fun = new Arr_fun();
        int[] arr = new int[] { -1, 2, -3, 5 };
        int q = arr_fun.MinSum(arr);
        int expected = 4;
        assertEquals(expected,q);
    }

    @Test
    public void arithMean() {
        Arr_fun arr_fun = new Arr_fun();
        int[] arr = new int[] { -1, 2, -3, 5 };
        int q = arr_fun.ArithMean(arr);
        int expected = 2;
        assertEquals(expected,q);
    }
}

public class Arr_fun {

    public static int Mins(int [] arr)
    {
        int q = 0;
        for(int i = 0; i < arr.length ; i++)
        {
            if(arr[i] < 0)
            {
                q++;
            }
        }
        return q;
    }
    public static  int MinSum(int[] arr)
    {
        int sum = 0;

        int min = arr[0];
        int min_id = 0;

        for(int i = 0; i < arr.length ; i++)
        {
            if(Math.abs(min) > Math.abs(arr[i]))
            {
                min_id = i;
            }
        }

        try
        {
            if (min_id == arr.length-1)throw new Exception("Помилка. Cуму вичислити не можливо.");
            {
                for(int j = min_id+1; j < arr.length ; j++)
                {
                    sum += arr[j];
                }

            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        return sum;
    }

    public static  int ArithMean(int[] arr)
    {
        int arth = 0;

        int min = arr[0];
        int min_id = 0;
        int max = min;
        int max_id =min_id;

        for(int i = 0; i < arr.length ; i++)
        {
            if(min > arr[i])
            {
                min_id = i;
            }
        }

        for(int i = 0; i < arr.length ; i++)
        {
            if(max < arr[i])
            {
                max_id = i;
            }
        }

        arth = ( min_id + max_id ) / 2;

        return  arth;
    }

}
import java.util.Scanner;

public class Program
    {
        public static void main (String [] args )
        {
            Scanner in = new Scanner(System.in);
            Scanner sc = new Scanner(System.in);
            char of;
            System.out.print("чи ви хочете користуватись програмою y/n ? ");
            of = sc.next().charAt(0);
            while (of == 'y')
            {
                System.out.println("Ведіть номер функції від 1-3 : ");
                int func = in.nextInt();
                switch (func)
                {
                    case 1:
                        System.out.print("Ведіть кількість елементів в  масиві  : ");
                        int size = in.nextInt();
                        int[] arr = new int[size];
                        Create(arr ,size ,1);
                        Output(arr);
                        fun1(arr);
                        break;
                    case 2:
                        System.out.print("Ведіть кількість елементів в  масиві  : ");
                        int size_ = in.nextInt();
                        int[] arr_ = new int[size_];
                        Create(arr_ ,size_ ,1);
                        Output(arr_);
                        fun2(arr_);
                        break;
                    case 3:
                        System.out.print("Ведіть кількість елементів в  масиві  : ");
                        int size_1 = in.nextInt();
                        int[] arr_1 = new int[size_1];
                        Create(arr_1 ,size_1 ,1);
                        Output(arr_1);
                        fun3(arr_1);
                        break;

                    default:
                        System.out.println("  Під таким номером функції не існує . ");
                }
                System.out.println("чи ви хочете користуватись програмою y/n ? ");
                of = sc.next().charAt(0);
            }
        }



        public static void  Create(int [] arr , int size , int s)
        {   Scanner in = new Scanner(System.in);

            switch (s)
            {

                case 1:
                    System.out.println(" Введення елементі масива за допомогою генератора псевдовипадкових чисел з відрізка [-100; 100] ");
                    for (int i = 0; i < size; i++)
                    {
                        arr[i] = (int) Math.round((Math.random() * 201) - 100);
                    }

                    break;
                case 2:
                    System.out.println(" Введення елементі масива з клавіатури ");
                    for (int i = 0; i < size; i++)
                    {
                        System.out.println("Ведіть елемент масиві  : ");
                        arr[i] = in.nextInt();
                    }

                    break;
                default:
                    break;
            }
        }
        static  void  Output(int [] arr)
        {
            for (int i = 0; i< arr.length;i++){
                System.out.print("  " + arr[i] + "  ");
            }
            System.out.println("  ");
        }
        static void fun1(int [] arr)
        {
            Arr_fun arr_fun = new Arr_fun();
            int q = arr_fun.Mins(arr);
            System.out.println("Мінімальне число в масиві : " + q);
        }
        private static void fun2(int[] arr_)
        {
            Arr_fun arr_fun = new Arr_fun();
            int sum = arr_fun.MinSum(arr_);
            System.out.println("Кількіть відємних елементів в масиві : " + sum);
        }
        private static void fun3(int[] arr_1)
        {
            Arr_fun arr_fun = new Arr_fun();
            int sum = arr_fun.ArithMean(arr_1);
            System.out.println("середнє арифметичне індексів максимального та мінімального елементів : " + sum);
        }
    }

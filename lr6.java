// metereo

import java.util.*;

public class metereo
{
    private int year;
    private int mouns;
    private  int day;
    private int temp;
    private int pressure;

    private static final String TableLine = "———————————————————————————————————————————————————————————————————————————————————————————————————————————————";
    private static final String TableLineGroup = "---------------------------------------------------------------------------------------------------------------";




    private static void OutputTableLine()
    {
        System.out.println(TableLine);
    }

    public metereo() {}

    public metereo (int year,int mouns,int day,int temp, int pressure)
    {
        this.year = year;
        this.mouns = mouns;
        this.day = day;
        this.temp = temp;
        this.pressure = pressure;
    }

    public void Input()
    {   Scanner in = new Scanner(System.in);
        boolean error = false;
        do
        {
            try{

                System.out.print("\tДень: ");
                day =  Integer.parseInt(in.nextLine());
                if(day > 32 && day < 0)throw new ArithmeticException("Помилка . Кількість днів в місяці не можуть бути білше ніж 31 і менше 1.");
                {
                    System.out.print("\tМісяць: ");
                    mouns =  Integer.parseInt(in.nextLine());
                    if(mouns > 13 && mouns < 0)throw new ArithmeticException("Помилка . Кількість місяців в рокі не можуть бути більше ніж 12 і менше ніж 1.");
                    {
                        System.out.print("\tРік: ");
                        year = in.nextInt();
                    }
                }
                System.out.print("\tВведіть темпетуру: ");
                temp =  Integer.parseInt(in.nextLine());
                if( temp < -273 ) throw new ArithmeticException("Помилка . Температура не може бути нище ніж 273 С ");
                {
                    System.out.print("\tВведіть атмосферний тиск: ");
                    pressure = Integer.parseInt(in.nextLine());
                    if( temp < 0 ) throw new ArithmeticException("Помилка .Тиск не може бути нижще 0");
                }
            }
            catch (ArithmeticException ex)
            {
                System.out.println(ex.getMessage());
            }
        }while (!error);
    }

    public void PrintFormat()
    {
        System.out.format("%-20s | %-20s | %-20s | %-20s | %-20s ", day,mouns,year,temp,pressure);
        System.out.println();
    }
    public static void PrintTable( metereo [] arr)
    {
        OutputTableLine();
        System.out.printf("%-20s | %-20s | %-20s | %-20s | %-20s ", "День","Місяць","Рік", "Температура", "Тиск");
        System.out.println();
        OutputTableLine();
        for(metereo item: arr) {
            item.PrintFormat();
        }
        OutputTableLine();
    }
    public static double Avg (metereo [] arr)
    {
        int sum = 0 ;
        float avg ;

        for (int i = 0; i < arr.length; i++)
        {
            sum += arr[i].temp;
        }
        avg = sum/ arr.length;
        return  avg;
    }
    public static  void PrintAvg(metereo [] arr)
    {
        double avg = Avg(arr);
        int i = 0;
        OutputTableLine();
        System.out.printf("%-20s | %-20s | %-20s | %-20s | %-20s ", "День","Місяць","Рік", "Температура", "Тиск"); System.out.println();
        OutputTableLine();

        for(metereo item : arr)
        {
            if(avg < arr [i].temp )
            {
                item.PrintFormat();
            }
            i++;
        }
        OutputTableLine();
    }
}

//cashtickets

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.IntFunction;

public class cashtickets extends ArrayList<cashtickets> {

    private  String destination;
    private  String date_out;
    private Time time_out;
    private  String date_in;
    private  Time time_in;
    private  int price;

    private static final String TableLine = "———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————";
    public static final SimpleDateFormat DateFormatter = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);


    public cashtickets() {}


    private static void OutputTableLine()
    {
        System.out.println(TableLine);
    }
    public  static  void cashtickets () {}

    public cashtickets( String destination, String date_out, Time time_out, String date_in,Time time_in, int price)
    {
        this.destination = destination;
        this.date_out = date_out;
        this.time_out = time_out;
        this.date_in = date_in;
        this.time_in = time_in;
        this.price = price;
    }

    public static void WriteToFile(cashtickets tickets) {
    }


    public String getDestination()
    {
        return destination;
    }
    public String getDate_out()
    {
        return date_out;
    }
    public Time getTime_out()
    {
        return time_out;
    }
    public String getDate_in()
    {
        return  date_in;
    }

    public Time getTime_in()
    {
        return time_in;
    }
    public int getPrice()
    {
        return price;
    }

    public void setDestination(String destination)
    {
        this.destination =destination;
    }

    public  void  setDate_out(String date_out)
    {
        this.date_out = date_out;
    }
    public void setTime_out(Time time_out)
    {
        this.time_out = time_out;
    }

    public void setDate_in(String date_in)
    {
        this.date_in = date_in;
    }

    public void setTime_in(Time time_in)
    {
        this.time_in = time_in;
    }
    public void setPrice(int price)
    {
        this.price = price;
    }
    public void Input()
    {
        Scanner in = new Scanner(System.in);
        boolean error = true;
        do
        {
            try{

                int ho,mi,se;

                System.out.print("\tВведіть назву пункту призначення : ");
                destination = in.nextLine();

                System.out.print("\tВведіть дату відбуття ");
                date_out = String.valueOf(in.nextInt());


                System.out.println("\tВведіть час відбуття ");
                System.out.print("\tГодину : ");
                ho = in.nextInt();
                System.out.print("\tХвелини : ");
                mi = in.nextInt();
                System.out.print("\tСекунди : ");
                se = in.nextInt();
                time_out =  new Time(ho - 0, mi -0, se);

                System.out.print("\tВведіть дату прибутя ");
                date_out = String.valueOf(in.nextInt());

                System.out.println("\tВведіть час прибутя ");
                System.out.print("\tГодину : ");
                ho = in.nextInt();
                System.out.print("\tХвелини : ");
                mi = in.nextInt();
                System.out.print("\tСекунди : ");
                se = in.nextInt();
                time_in = new Time(ho -0, mi-0, se-0);

                System.out.print("\tВведіть ціну квитка : ");
                price = in.nextInt();

                error = false;
            }
            catch (ArithmeticException ex)
            {
                System.out.println(ex.getMessage());
            }
        }while (error);
    }
    public void PrintFormat()
    {
        System.out.format("%-20s | %-20s | %-20s | %-20s | %-20s | %-20d",
                destination, date_out, time_out , date_in, time_in , price);
        System.out.println();
    }
    public static void PrintTable(List<cashtickets> arr)
    {
        OutputTableLine();
        System.out.format("%-20s | %-20s | %-20s | %-20s | %-20s | %-20s","destination", "date out", "time out" , "date in", "time in ", "price");
        System.out.println();
        OutputTableLine();

        for(int i = 0; i < arr.toArray().length ; i++)
        {
            arr.get(i).PrintFormat();
        }
        OutputTableLine();
    }

}
// cashticketsfile
import java.sql.Time;
import java.util.*;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class cashticketsfile
{
    private static final String fileName = "Library.txt";

    private static void WriteToFile(BufferedWriter bufferedWriter, cashtickets tickets) throws Exception
    {
        bufferedWriter.write("Бібліотека:" + "\n");
        bufferedWriter.write(tickets.getDestination() + "\n");
        bufferedWriter.write(tickets.getDate_out() + "\n");
        bufferedWriter.write(tickets.getTime_out() + "\n");
        bufferedWriter.write(tickets.getDate_out() + "\n");
        bufferedWriter.write(tickets.getTime_in() + "\n");
        bufferedWriter.write(tickets.getPrice() + "\n");
    }

    public static void WriteToFileList(ArrayList<cashtickets> tickets, boolean append)
    {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,append)))
        {
            for (cashtickets cashtickets: tickets)
            {
                WriteToFile(bufferedWriter, cashtickets);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void WriteToFile(cashtickets tickets, boolean append)
    {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,append))) {
            WriteToFile(bufferedWriter, tickets);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void WriteToFile(cashtickets tickets)
    {
        WriteToFile(tickets, true);
    }
    public static void RewriteFile(ArrayList<cashtickets> tickets)
    {
        WriteToFileList(tickets, false);
    }

    public static void RemoveFromFile(int index) throws Exception {
        try {
            ArrayList<cashtickets> tickets = ReadFromFile();

            tickets.remove(index);

            RewriteFile(tickets);

        } catch (Exception e) {
            throw e;
        }
    }

    public static ArrayList<cashtickets> ReadFromFile() throws Exception {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            ArrayList<cashtickets> libraries = new ArrayList();

            String line = bufferedReader.readLine();

            while(line != null)
            {
                cashtickets library = new cashtickets();
                line = bufferedReader.readLine();
                library.setDestination(String.valueOf(line));

                line = bufferedReader.readLine();
                library.setDate_out(line);

                line = bufferedReader.readLine();
                library.setTime_out(Time.valueOf(line));

                line = bufferedReader.readLine();
                library.setDate_in(line);

                line = bufferedReader.readLine();
                library.setTime_in(Time.valueOf(line));

                line = bufferedReader.readLine();
                library.setPrice(Integer.parseInt(line));

                libraries.add(library);

                line = bufferedReader.readLine();
            }
            return libraries;

        } catch (FileNotFoundException e) {
            throw new Exception("Файл не існує!");
        } catch (IOException e) {
            throw new Exception("Помилка: " + e.getMessage());
        }
    }

    public static ArrayList<cashtickets> Sort() throws Exception {
        ArrayList<cashtickets> list;
        try {
            list = ReadFromFile();
            boolean sorted = false;

            while (!sorted) {
                sorted = true;
                for (int i = 0; i < list.size()-1; i++)
                {
                    if (list.get(i).getTime_in().getHours() > list.get(i+1).getTime_in().getHours() )
                    {
                        if (list.get(i).getTime_in().getMinutes() > list.get(i+1).getTime_in().getMinutes() )
                        {
                            if (list.get(i).getTime_in().getSeconds() > list.get(i+1).getTime_in().getSeconds() )
                            {
                                cashtickets temp = list.get(i);
                                list.set(i, list.get(i + 1));
                                list.set(i + 1, temp);
                                sorted = false;
                            }
                        }
                    }
                }
            }

            return list;

        } catch (Exception e) {
            throw e;
        }
    }

    public static ArrayList<cashtickets> Search(String key) throws Exception
    {
        try {
            ArrayList<cashtickets> read = ReadFromFile();
            ArrayList<cashtickets> cash = new ArrayList<>();

            for(cashtickets item: read) {
                if(item.getDestination().contains(key))
                {
                    cash.add(item);
                }
            }
            return cash;
        } catch (Exception e) {
            throw e;
        }
    }
}
// main

import java.sql.Time;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{   public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        int q;
        char of;
        System.out.println(" Завдання 1 або Завдання 2 .");
        q = in.nextInt();
        switch (q)
        {case 1:
            int d;
            metereo met1 = new metereo(2011,5,13,13,700);
            metereo met2 = new metereo(2002,11,2,16,804);
            metereo met3 = new metereo(2006,10,6,7,640);
            metereo met4 = new metereo(2004,3,4,10,340);
            metereo met5 = new metereo(2002,5,7,20,840);
            metereo met6 = new metereo(2006,10,16,16,658);
            metereo met7 = new metereo(2006,10,6,27,640);
            metereo met8 = new metereo(2004,6,4,10,340);
            metereo met9 = new metereo(2002,9,22,26,840);
            metereo met10 = new metereo(2006,11,16,16,658);
            metereo [] mete = {met1,met2,met3,met4,met5,met6,met7,met8,met9,met10};
            do {
                System.out.println("1 Вивести інформацію про метеорологічні спостереження .");
                System.out.println("2 Вивести метеорологічні спостереження в яких температурва більша за середню .");
                System.out.println("3 Вихід .");
                d = in.nextInt();
                switch (d)
                {
                    case 1:
                        metereo.PrintTable( mete);
                        break;
                    case 2:
                        metereo. PrintAvg(mete);
                        break;
                    default:
                        break;
                }
            }while (d < 3);
            case 2:
            do
            {
                System.out.println("Бібліотека. Операції:");
                System.out.println("д — додати записи;");
                System.out.println("р — редагувати запис;");
                System.out.println("в — видалити запис;");
                System.out.println("т — вивести інформацію на екран;");
                System.out.println("п — пошук по автору;");
                System.out.println("с — сортувати за роками;");
                System.out.println("- — завершити");
                System.out.print("Операція: ");
                of = sc.next().charAt(0);
                System.out.println();

                switch (of)
                {
                    case 'д':
                        System.out.println("Додавання: ");
                        AddInfo();
                        break;
                    case 'р':
                        System.out.println("Редагування: ");
                        EditInfo();
                        break;
                    case 'в':
                        System.out.println("Видалення: ");
                        DeleteInfo();
                        break;
                    case 'т':
                        System.out.println("Виведення:");
                        PrintInfo();
                        break;
                    case 'п':
                        System.out.println("Пошук: ");
                        SearchInfo();
                        break;
                    case 'с':
                        System.out.println("Сортувати: ");
                        SortInfo();
                        break;
                    case '-':
                        return;
                    default:
                        System.out.println("Даного завдання не існує!");
                        break;
                }
                System.out.println();
            } while (true);

            default:
                break;
        }
    }
    public static void AddInfo()
    {
        Scanner in = new Scanner(System.in);
        int n;
        do {
            System.out.print("Введіть кількість нових записів: ");
            n = in.nextInt();

            if(n <= 0) System.out.println("Кількість записів має бути більше 0.");
        }
        while (n <= 0);

        System.out.println("Введення Квитка :");
        for (int i = 0; i < n; i++)
        {
            System.out.println("Квиток " + (i+1) + ":");
            cashtickets tickets = new cashtickets();
            tickets.Input();
            cashticketsfile.WriteToFile(tickets);
        }
    }
    public static void EditInfo()
    {
        try {
            ArrayList<cashtickets> tickets = cashticketsfile.ReadFromFile();

            Scanner in = new Scanner(System.in);
            int n;
            do {
                System.out.print("Введіть номер квитка для редагування: ");
                n = in.nextInt();

                if(n < 0 || n >= tickets.size()) System.out.println("Дані введено неккоректно.");
            }
            while (n < 0 || n >= tickets.size());

            System.out.println("Квиток:");
            cashtickets tick = new cashtickets();
            tick.Input();
            tick.set(n, tick);

            cashticketsfile.RewriteFile(tickets);
            System.out.println("Квиток змінено!");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void DeleteInfo()
    {
        try {
            ArrayList<cashtickets> tickets = cashticketsfile.ReadFromFile();

            Scanner in = new Scanner(System.in);
            int n;
            do {
                System.out.print("Введіть номер квитка для видалення: ");
                n = in.nextInt();

                if(n < 0 || n >= tickets.size()) System.out.println("Дані введено неккоректно.");
            }
            while (n < 0 || n >= tickets.size());

            cashticketsfile.RemoveFromFile(n);
            System.out.println("квиток видалено!");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void PrintInfo()
    {
        try {
            List<cashtickets> tickets = cashticketsfile.ReadFromFile();
            cashtickets.PrintTable(tickets);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public static void SearchInfo()
    {
        Scanner in = new Scanner(System.in);
        String key;
        System.out.println("Ключ пошуку: ");
        key = in.nextLine();

        try {
            List<cashtickets> tickets = cashticketsfile.Search(String.valueOf(key));

            if(tickets.size() > 0)
            {
                cashtickets.PrintTable(tickets);
            }
            else
            {
                System.out.println("Не знайдено!");
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void SortInfo()
    {
        try {
            List<cashtickets> tickets = cashticketsfile.Sort();

            if(tickets.size() > 0)
            {
                cashtickets.PrintTable(tickets);
            }
            else
            {
                System.out.println("Записів не знайдено!");
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
// metereoTest
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class metereoTest {

    @org.junit.Test
    public void avg()
    {
        metereo met1 = new metereo(3,3,3,3,3);
        metereo met2 = new metereo(2002,31,2,6,8);
        metereo met3 = new metereo(2006,14,6,6,6);
        metereo [] mete = {met1,met2,met3};

        double q = metereo.Avg(mete);
        double expected = 70;
        assertEquals(expected,q,q);
    }
}

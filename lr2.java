package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);

        int s;
        int d;
        char of;
        System.out.println("чи ви хочете користуватись програмою y/n ? ");
        of = sc.next().charAt(0);
        while (of=='y') {
            System.out.println("Ведіть номер функції від 1-4 : ");
            d = in.nextInt();

            switch (d) {
                case 1:
                    boolean error = true;
                    fun1(error);

                    break;
                case 2:
                    System.out.println("for or while or dowhile ?");
                    s = in.nextInt();
                    fun2(s);
                    break;
                case 3:
                    System.out.println("for or while or dowhile ?");
                    s = in.nextInt();
                    System.out.println("Ведіть  x : ");
                    int x = in.nextInt();
                    System.out.println("Ведіть n : ");
                    int n = in.nextInt();

                    fun3(n, x, s);
                    break;
                case 4:
                    System.out.println("for or while or dowhile ?");
                    s = in.nextInt();

                    fun4(s);
                    break;
                default:
                    System.out.println("das not exist");
            }
            System.out.println("чи ви хочете користуватись програмою y/n ? ");
            of = sc.next().charAt(0);
        }
    }
    static void fun1(boolean error)
    {
        Scanner in = new Scanner(System.in);
        do {
            try {
                System.out.print("Введіть x: ");
                long x = in.nextLong();
                System.out.print("Введіть z: ");
                long z = in.nextLong();

                float div = (float) (Math.sqrt(x) - 2 * Math.sqrt(x * z));
                if(div == 0) throw new ArithmeticException("Для заданих значень у обчислюваному виразі виконується ділення на 0");

                float a = (float) (Math.sqrt(x) + 2 * Math.sqrt(x * z)) / div;
                String res = String.format("%.2f",a);
                System.out.println("Результат: " + res);
                error = false;
            }
            catch (ArithmeticException ex) {
                System.out.println(ex.getMessage());
            }
        } while (error);
    }

    /*Обчислити добуток всіх чисел, кратних 5, у діапазоні [20;80].*/
    static void fun2(int s)
    {    switch (s)
        {
        case 1 :
            double sum = 1;
            for (int i = 20; i <= 80; i++)
            {
                if (i % 5 == 0) {
                    sum *= i;
                }
            }
            System.out.println("Добуток елеменів кратних 5 : " + sum);
            break;
        case 2 :
            double su = 1;
            int a = 20;
            while (a <= 80) {
                if (a % 5 == 0)
                {
                    su *= a;
                }
                a++;
            }
            System.out.println("Добуток елеменів кратних 5 : " + su);
            break;
        case 3 :
            double r = 1;
            int b = 20;
            do {
                if (b % 5 == 0) {
                    r *= b;
                }
                b++;
            } while (b <= 80);
            System.out.println("Добуток елеменів кратних 5 : " + r);
            break;
        }
    }
    /*Задані натуральне число n та дійсне число x */
    static void fun3(int x , int n , int s)
    {
            switch (s) {
                case 1:
                    int sum = 0;
                    for (int i = 1; i < n; i++)
                    {
                        for (int j = 2; j < n; j++)
                        {
                            sum +=  (x + j);
                        }
                    }
                    System.out.println("Сума функціонального ряда  дорівнює : " + sum);
                    break;
                case 2:
                    int a = 1;
                    int b = 2;
                    int su = 0;
                    while (a < n)
                    {    b = 2;
                        while(b < n)
                        {
                           su += (x + b);
                           b++;
                        }
                        a++;
                    }
                    System.out.println("Сума функціонального ряда дорівнює : " + su);
                    break;
                case 3:
                    int c = 1;
                    int d = 2;
                    int z = 0;
                    do
                    {    d = 2;
                        do
                        {
                            z += (x + d);
                            d++;
                        }while(d < n);
                        c++;
                    }while (c < n);
                    System.out.println("Сума функціонального ряда дорівнює : " + z);
                    break;

            }
    }


    /*Результати обчислення функції y = f(x) на проміжку [a,b] з кроком dx ;
    y = x ^ 3 , a = -6 , b = 3 , dx =0.75 */
    static void fun4(int s)
    {
        double b = 3;
        double a = -6;
        double dx = 0.75;
        switch (s)
        {
            case 1:
                System.out.println("Результати обчислення функції на проміжку від -6 до 3 з проміжком 0.75");
                for (double i = a; i <= b; i += dx) {
                    double y = Math.pow(i, 3);
                    String res = String.format("%.2f",y);
                    System.out.println("y = " + res + " x = " + i);
                }
                break;
            case 2:
                System.out.println("Результати обчислення функції на проміжку від -6 до 3 з проміжком 0.75");
                double j = a;
                while (j <= b) {
                    double y = Math.pow(j, 3);
                    String res = String.format("%.2f",y);
                    System.out.println("y = " + res + " x = " + j);
                    j += dx;
                }
                break;
            case 3:
                System.out.println("Результати обчислення функції на проміжку від -6 до 3 з проміжком 0.75");
                double d = a;
                do {
                    double y = Math.pow(d, 3);
                    String res = String.format("%.2f",y);
                    System.out.println("y = " + res + " x = " + d);
                    d += dx;
                } while (d <= b);
                break;
        }
    }
}

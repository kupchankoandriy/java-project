// Main

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void AddInfo() {
        Scanner in = new Scanner(System.in);
        int n;
        do {
            System.out.print("Введіть кількість нових записів: ");
            n = in.nextInt();

            if (n <= 0) System.out.println("Кількість записів має бути більше 0.");
        }
        while (n <= 0);

        System.out.println("Введення лекції : ");
        for (int i = 0; i < n; i++) {
            System.out.println("Лекція " + (i + 1) + ":");
            Lection practical = new Lection();
            practical.Input();
            LectionFile.WriteToFile(practical);
        }
    }

    public static void EditInfo() {
        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();

            Scanner in = new Scanner(System.in);
            int n;
            do {
                System.out.print("Введіть номер лекції для редагування: ");
                n = in.nextInt();

                if (n < 0 || n >= lections.size()) System.out.println("Дані введено неккоректно.");
            }
            while (n < 0 || n >= lections.size());

            System.out.println(" Лекція :");
            Lection lection = new Lection();
            lection.Input();
            lections.set(n, lection);

            LectionFile.RewriteFile(lections);
            System.out.println(" Лекція змінено!");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void DeleteInfo() {
        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();

            Scanner in = new Scanner(System.in);
            int n;
            do {
                System.out.print("Введіть номер лекції для видалення: ");
                n = in.nextInt();

                if (n < 0 || n >= lections.size()) System.out.println("Дані введено неккоректно.");
            }
            while (n < 0 || n >= lections.size());

            LectionFile.RemoveFromFile(n);
            System.out.println("Лекція видалено!");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void PrintInfo() {
        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();
            Lection.PrintTable(lections);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void AVGInfo() {
        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();
            if (lections.size() <= 0) throw new Exception("Немає записів");

            System.out.println("Середнє значення: " + Lection.AVG(lections));

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void MaxStudInfo() {
        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();
            if (lections.size() <= 0) throw new Exception("Немає записів");

            Lection.PrintTable(Lection.MaxStudentCountList(lections));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void CountPew() {
        Scanner in = new Scanner(System.in);

        try {
            ArrayList<Lection> lections = LectionFile.ReadFromFile();
            if (lections.size() <= 0) throw new Exception("Немає записів");
            int k = 0;
            for(int i = 0 ; i < lections.size();i++)
            {
                String[] s2 = lections.get(i).getPew().split("\\s+");
                for (int j= 0; i < s2.length-1; j++) {
                    if (s2[j].length() == 0) {
                        continue;
                    }
                    if (s2[j].charAt(0) <= 'z' && s2[j].charAt(0) >= 'a'&& s2[j].charAt(0) <= '0'&& s2[j].charAt(0) >= '9')
                    {
                        k++;
                    }
                }
                System.out.printf("Num Count = %d ", k);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String sel;
        do {
            System.out.println("Практичні завдання. Операції:");
            System.out.println("д — додати записи;");
            System.out.println("р — редагувати запис;");
            System.out.println("в — видалити запис;");
            System.out.println("т — вивести інформацію на екран;");
            System.out.println();
            System.out.println("с — середня кількість студентів;");
            System.out.println("з — вивід занять з максимальною кількістю студентів;");
            System.out.println("п — кількість слів в назві кафедрі ;");
            System.out.println("- — завершити");
            System.out.print("Операція: ");
            sel = in.next();
            System.out.println();

            switch (sel) {
                case "д":
                    System.out.println("Додавання: ");
                    AddInfo();
                    break;
                case "р":
                    System.out.println("Редагування: ");
                    EditInfo();
                    break;
                case "в":
                    System.out.println("Видалення: ");
                    DeleteInfo();
                    break;
                case "т":
                    System.out.println("Виведення:");
                    PrintInfo();
                    break;
                case "с":
                    System.out.println("Середня кількість студентів: ");
                    AVGInfo();
                    break;
                case "з":
                    System.out.println("Заняття з максимальною кількістю студентів: ");
                    MaxStudInfo();
                    break;
                case "п":
                    System.out.println("список тем з певним словом у назві: ");
                    CountPew();
                    break;
                case "-":
                    return;
                default:
                    System.out.println("Даного завдання не існує!");
                    break;
            }
            System.out.println();
        } while (true);
    }
}
// EdCourse
public abstract class EdCourse
{
    protected String name;
    protected String pew;

    public abstract void PrintFormat();
    public abstract void Input();

    public String getName()
    {
        return name;
    }
    public String getPew()
    {
        return pew;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setPew(String pew)
    {
        this.pew = pew;
    }
}
//  Lection

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Lection extends EdCourse
{

    private Date date;
    private int group;
    private int studentscount;

    private static final String TableLine = "—————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————";
    private static void OutputTableLine()
    {
        System.out.println(TableLine);
    }
    public Date getDate()
    {
        return date;
    }

    public int getGroup()
    {
        return group;
    }

    public int getStudentsCount()
    {
        return studentscount;
    }
    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setGroup(int group)
    {
        this.group = group;
    }

    public void setStudentsCount(int studentscount)
    {
        this.studentscount = studentscount;
    }
    public Lection() {}
    public Lection (String name, String pew, Date date, int group,int studentscount)
    {
        this.name = name;
        this.pew = pew;
        this.date = date;
        this.group = group;
        this.studentscount = studentscount;
    }
    public void PrintFormat()
    {
        System.out.format("%-30s | %-7s | %-10s | %-80s | %-4d",
                name, pew, LectionFile.DateFormatter.format(date), group, studentscount);
        System.out.println();
    }

    public static void PrintTable(ArrayList<Lection> arr)
    {
        OutputTableLine();
        System.out.printf("%-30s | %-20s | %-20s | %-20s | %-20s", "Назва", "Кафедра", "Дата", "Група", "К-ть студ.");
        System.out.println();
        OutputTableLine();
        for(Lection item: arr) {
            item.PrintFormat();
        }
        OutputTableLine();
    }
    public void Input() {
        Scanner in = new Scanner(System.in);

        System.out.print("Назва: ");
        name = in.nextLine();

        boolean Error = true;
        do {
            try {
                System.out.print(" Назва Кафедри : ");
                pew = in.nextLine();
                if(pew == "")throw new Exception("Кафедру ведення не коректно . ");
                Error = false;
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        } while (Error);

        Error = true;
        do {
            try {
                System.out.print("Дата: ");
                String input = in.nextLine();
                date = LectionFile.DateFormatter.parse(input);

                Error = false;
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        } while (Error);


        System.out.print("Номер Групи : ");
        group = Integer.parseInt(in.nextLine());



        Error = true;
        do {
            try {
                System.out.print("Кількість студентів: ");
                studentscount = Integer.parseInt(in.nextLine());
                if(studentscount <= 1) throw new Exception("Кількість студентів повинна бути більша за 1");

                Error = false;
            }
            catch (NumberFormatException e) {
                System.out.println("Помилка при введені. Невірний тип. Спробуйте ще раз!");
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        } while (Error);

    }
    public static int AVG(ArrayList<Lection> arr)
    {
        int stud_count = 0;
        for(Lection lection: arr)
        {
            stud_count+=lection.getStudentsCount();
        }
        return stud_count / arr.size();
    }
    public static ArrayList<Lection> MaxStudentCountList(ArrayList<Lection> arr)
    {
        int max = arr.get(0).getStudentsCount();

        for(Lection  lection : arr)
        {
            if(max < lection.getStudentsCount())
            {
                max = lection.getStudentsCount();
            }
        }

        ArrayList<Lection> newArr = new ArrayList<>();

        for(Lection lection : arr)
        {
            if(lection.getStudentsCount() == max)
                newArr.add(lection);
        }

        return newArr;
    }
}
// LectionFile

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LectionFile
{
    private static final String fileName = "Practical.txt";
    public static final SimpleDateFormat DateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    private static void WriteToFile(BufferedWriter bufferedWriter, Lection lection) throws IOException {
        bufferedWriter.write("Практичне завдання:" + "\n");
        bufferedWriter.write(lection.getName() + "\n");
        bufferedWriter.write(lection.getPew() + "\n");
        bufferedWriter.write(DateFormatter.format(lection.getDate())+ "\n");
        bufferedWriter.write(lection.getGroup() + "\n");
        bufferedWriter.write(lection.getStudentsCount() + "\n");
    }

    public static void WriteToFileList(ArrayList<Lection> lections, boolean append) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,append))) {
            for (Lection lection: lections)
            {
                WriteToFile(bufferedWriter, lection);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void WriteToFile(Lection lection, boolean append) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,append))) {
            WriteToFile(bufferedWriter, lection);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void WriteToFile(Lection lection)
    {
        WriteToFile(lection, true);
    }

    public static void RewriteFile(ArrayList<Lection> lections) {
        WriteToFileList(lections, false);
    }

    public static void RemoveFromFile(int index) throws Exception {
        ArrayList<Lection> lections = ReadFromFile();

        lections.remove(index);
        RewriteFile(lections);
    }

    public static ArrayList<Lection> ReadFromFile() throws Exception {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            ArrayList<Lection> lections = new ArrayList<>();

            String line = bufferedReader.readLine();

            while(line != null)
            {
                Lection lection = new Lection();

                line = bufferedReader.readLine();
                lection.setName(line);

                line = bufferedReader.readLine();
                lection.setPew(line);

                line = bufferedReader.readLine();
                lection.setDate(DateFormatter.parse(line));

                line = bufferedReader.readLine();
                lection.setGroup(Integer.parseInt(line));

                line = bufferedReader.readLine();
                lection.setStudentsCount(Integer.parseInt(line));

                lections.add(lection);

                line = bufferedReader.readLine();
            }
            return lections;

        } catch (FileNotFoundException e) {
            throw new Exception("Файл не існує!");
        } catch (IOException e) {
            throw new Exception("Помилка: " + e.getMessage());
        }
    }

    public static ArrayList<Lection> Sort() throws Exception {
        ArrayList<Lection> list;
        list = ReadFromFile();
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < list.size()-1; i++) {
                if (list.get(i).getStudentsCount() < list.get(i + 1).getStudentsCount()) {
                    Lection temp = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, temp);
                    sorted = false;
                }
            }
        }

        return list;
    }

    public static ArrayList<Lection> Search(String key) throws Exception {
        ArrayList<Lection> read = ReadFromFile();
        ArrayList<Lection> practicals = new ArrayList<>();

        for(Lection item: read) {
            if(item.getName().contains(key))
            {
                practicals.add(item);
            }
        }
        return practicals;
    }
}
// LectionTest
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LectionTest {

    @org.junit.jupiter.api.Test
     void AVG()
    {
        Lection lection1 = new Lection("err","t34",new Date(),32,22);
        Lection lection2 = new Lection("esr","t4",new Date(),23,10);
        Lection lection3 = new Lection("ewq","tj4",new Date(),123,1);

        ArrayList<Lection> list = new ArrayList<>();
        list.add(lection1);
        list.add(lection2);
        list.add(lection3);

        int actual = Lection.AVG(list);
        int expected = 11;
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void MaxStudentCountList()
    {
        Lection lection1 = new Lection("err","t34",new Date(),32,22);
        Lection lection2 = new Lection("esr","t4",new Date(),23,10);
        Lection lection3 = new Lection("ewq","tj4",new Date(),123,1);

        ArrayList<Lection> list = new ArrayList<>();
        list.add(lection1);
        list.add(lection2);
        list.add(lection3);

        int actual = Lection.MaxStudentCountList(list).get(0).getStudentsCount();
        int expected = 22;
        assertEquals(expected, actual);
    }
}

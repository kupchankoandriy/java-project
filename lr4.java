package com.company;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);

        char of;
        System.out.print("чи ви хочете користуватись програмою y/n ? ");
        of = sc.next().charAt(0);
        while (of == 'y') {
            System.out.print("Ведіть номер функції від 1,2 : ");
            int func = in.nextInt();
            switch (func) {
                case 1:
                    int[][] arr = new int[10][10];
                    System.out.print(" Введiть кiлкiсть колонок : ");
                    int column = in.nextInt();
                    System.out.print(" Введiть кiлкiсть рядкiв : ");
                    int rows = in.nextInt();
                    Create(arr, rows, column, 2);
                    System.out.println(" прямокутний масив ");
                    Output(arr, rows, column);
                    fun1(column, rows, arr);
                    break;
                case 2:
                    int[][] mas = new int[10][10];
                    System.out.print(" Введiть кiлкiсть колонок : ");
                    int col = in.nextInt();
                    System.out.print(" Введiть кiлкiсть рядкiв : ");
                    int row = in.nextInt();
                    Create(mas, row, col, 1);
                    System.out.println(" Масив до змін ");
                    Output(mas, row, col);
                    fun2(col, row, mas);
                    System.out.println(" Масив після змін ");
                    Output(mas, row, col);

                    break;
                default:
                    System.out.println("Під таким номером функції не існує .");
            }
            System.out.println("чи ви хочете користуватись програмою y/n ? ");
            of = sc.next().charAt(0);
        }
    }

    public static void Output(int[][] arr, int rows, int column) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print("  " + arr[i][j] + "  ");
            }
            System.out.println("\n");
        }
    }

    public static void Create(int[][] arr, int rows, int column, int s) {
        Scanner in = new Scanner(System.in);

        switch (s) {

            case 1:
                System.out.println(" Введення елементі масива за допомогою генератора псевдовипадкових чисел з відрізка [-100; 100] ");
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < column; j++)
                    {
                        arr[i][j] = (int) Math.round((Math.random() * 201) - 100);
                    }
                }


                break;
            case 2:
                System.out.println(" Введення елементі масива з клавіатури ");
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < column; j++) {
                        System.out.print("Ведіть елемент масиві   ряднка " + (i + 1) + " колонки " + (j + 1) + " : ");
                        arr[i][j] = in.nextInt();
                    }

                }

                break;
            default:
                break;
        }
    }

    static int Min(int rows, int[][] arr, int column, int index) {
        int min = arr[0][0];
        int min_id = 0;
        int dob = 1;
        for (int i = 1; i < rows; i++) {
            if (min > arr[i][index]) {
                min_id = i;
            }
        }
        for (int j = 0; j < column; j++) {
            dob *= arr[min_id][j];
        }
        return dob;
    }

    static int Max(int rows, int[][] arr, int column, int index) {
        int max = arr[0][0];
        int max_id = 0;
        int dob = 1;
        for (int i = 1; i < rows; i++) {
            if (max < arr[i][index]) {
                max_id = i;
            }
        }
        for (int j = 0; j < column; j++) {
            dob *= arr[max_id][j];
        }
        return dob;
    }

    static void fun1(int column, int rows, int[][] arr) {
        int[] b = new int[column * 2];

        for (int i = 0; i < column * 2; i++) {
            if(i == 0)
            {
                b[i] = Min(rows, arr, column, i);
            }
            if (i % 2 == 0 && i != 1)
            {
                b[i] = Min(rows, arr, column, i);
            }

        }
        for (int i = 0; i < column * 2; i++) {
            if (i % 2 != 0)
            {
                b[i] = Max(rows, arr, column, i);
            }

        }
        System.out.println("добутком елементів рядків, в яких знаходяться максимальний та мінімальний елементи");
        for (int i = 0; i < b.length; i++) {
            System.out.print("  " + b[i] + "  ");
        }
        System.out.println("\n");
    }

    static void fun2(int column, int rows, int[][] arr)
    {
        for (int i = 0; i < column; i++)
        {
            int[] newArr = new int[rows];
            for (int j = 0; j < rows; j++)
            {
                newArr[j] = arr[j][i];
            }

            int min = 0;
            int buf = 0;

            for (int q = 0; q < newArr.length; q++) {
                min = q;
                for (int j = q + 1; j < newArr.length; j++) {
                    min = (newArr[j] < newArr[min]) ? j : min;
                    if (q != min) {
                        buf = newArr[q];
                        newArr[q] = newArr[min];
                        newArr[min] = buf;
                    }
                }
            }
            for (int q = 0; q < newArr.length; q++) {
                arr[q][i] = newArr[q];
            }

        }

    }
}
